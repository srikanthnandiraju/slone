//App Basic Routings
Path.map("#/signup").to(function() {
	//$("#title_text").html("SIGNUP");
    $("#title_text").html("PICI DASHBOARD");
	$("#stage").load("screens/signup.html");
});
Path.map("#/forgot-password").to(function() {
	$("#title_text").html("PICI DASHBOARD");
	$("#stage").load("screens/forgot_password.html");
});

Path.map("#/profile").to(function() {
	$("#title_text").html("PROFILE");
	$("#stage").load("screens/account_details.html");
});

Path.map("#/login").to(function() {
	$("#title_text").html("LOGIN");
	$("#stage").load("screens/login.html");
});

//Clinician Routings
Path.map("#/clinician-home").to(function() {
	$("#title_text").html("CLINICIAN HOME");
	$("#stage").load("screens/clinician_home.html");
});

//Patient Routings
Path.map("#/patient-home").to(function() {
	$("#title_text").html("PATIENT HOME");
	$("#stage").load("screens/patient_home.html");
});

Path.map("#/patient-history").to(function() {
	$("#title_text").html("HISTORY");
	$("#stage").load("screens/patient_history.html");
});

Path.map("#/patient-careteam").to(function() {
	$("#title_text").html("CARE TEAM");
	$("#stage").load("screens/patient_careteam.html");
});

Path.map("#/patient-images").to(function() {
	$("#title_text").html("IMAGES");
	$("#stage").load("screens/patient_images.html");
});

Path.map("#/patient-medication").to(function() {
	$("#title_text").html("TREATMENTS");
	$("#stage").load("screens/patient_medication.html");
});

Path.map("#/patient-documents").to(function() {
	$("#title_text").html("DOCUMENTS");
	$("#stage").load("screens/patient_documents.html");
});

Path.map("#/patient-response").to(function() {
	$("#title_text").html("RESPONSE");
	$("#stage").load("screens/patient_response.html");
});

Path.map("#/patient-timeline").to(function() {
	$("#title_text").html("TIMELINE");
	$("#stage").load("screens/patient_timeline.html");
});

Path.map("#/patient-schedule").to(function() {
	$("#title_text").html("SCHEDULE");
	$("#stage").load("screens/patient_schedule.html");
});

//Bioinformatician routings
Path.map("#/bioinformatician-home").to(function() {
	$("#title_text").html("WORKLIST");
	//$("#stage").load("screens/bioinformatician_home1.html");
	$("#stage").load("screens/bioinformatician_worklist.html");
});

Path.map("#/bio-work-list").to(function() {
	$("#title_text").html("WORKLIST");
	$("#stage").load("screens/bioinformatician_worklist.html");
});

Path.map("#/research-report-form").to(function() {
	$("#title_text").html("RESEARCH REPORTS");
	$("#stage").load("screens/bio_research_report_form.html");
});

Path.map("#/research-reports").to(function() {
	$("#title_text").html("RESEARCH REPORTS");
	$("#stage").load("screens/bio_research_reports.html");
});

//Director Routings
Path.map("#/director-home").to(function() {
	$("#title_text").html("DIRECTOR DASHBOARD");
	$("#stage").empty().load("screens/director_home.html");
});

Path.map("#/study-view").to(function() {
	$("#title_text").html("SINGLE STUDY VIEW");
	$("#stage").empty().load("screens/full_study_view.html");
});
Path.map("#/single-subject-view").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").empty().load("screens/full_subject_view.html");
});

Path.map("#/single-patient-view").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").empty().load("screens/single_patient_full_view.html");
});

Path.map("#/cytokine-details").to(function() {
	$("#title_text").html("CYTOKINE");
	$("#stage").empty().load("screens/cytokine_screen.html");
});

Path.map("#/ihc-details").to(function() {
	$("#title_text").html("IHC");
	$("#stage").empty().load("screens/ihc_screen.html");
});

Path.map("#/flow-cytometry-details").to(function() {
	$("#title_text").html("FLOW CYTOMETRY");
	$("#stage").empty().load("screens/flow_cytometry_screen.html");
});
Path.map("#/adverse-events-details").to(function() {
		$("#title_text").html("ADVERSE EVENTS");
	$("#stage").empty().load("screens/adverse_screen.html");
});
Path.map("#/treatments-details").to(function() {
	$("#title_text").html("TREATMENTS");
	$("#stage").empty().load("screens/treatment_screen.html");
	
});
Path.map("#/collection-details").to(function() {
	$("#title_text").html("COLLECTION");
	$("#stage").empty().load("screens/collection_screen.html");
});
Path.map("#/clinical-labs-details").to(function() {
	$("#title_text").html("CLINICAL LABS");
	$("#stage").empty().load("screens/clinical_labs_screen.html");
});

Path.map("#/study-patients").to(function() {
	$("#title_text").html("STUDY PATIENTS");
	$("#stage").empty().load("screens/study_patients_screen.html");
});

Path.map("#/study-protocol").to(function() {
	$("#title_text").html("STUDY PROTOCOL");
	$("#stage").empty().load("screens/study_protocol_screen.html");
});

Path.map("#/study-response").to(function() {
	$("#title_text").html("STUDY RESPONSE");
	$("#stage").empty().load("screens/study_response_screen.html");
});

Path.map("#/study-adverse").to(function() {
	$("#title_text").html("STUDY ADVERSE EVENTS");
	$("#stage").empty().load("screens/study_adverse_screen.html");
});

Path.map("#/study-timeline").to(function() {
	$("#title_text").html("STUDY TIMELINE");
	$("#stage").empty().load("screens/study_timeline_screen.html");
});

Path.map("#/study-cohort").to(function() {
	$("#title_text").html("STUDY COHORTING");
	$("#stage").empty().load("screens/study_cohorting_screen.html");
});

//Researcher Routings
Path.map("#/researcher-home").to(function() {
	$("#title_text").html("STUDIES");
	$("#stage").load("screens/researcher_home1.html");
});

Path.map("#/research-datasets").to(function() {
	$("#title_text").html("DATA SETS");
	$("#stage").load("screens/researcher_studies_screen.html");
});

Path.map("#/research-requests").to(function() {
	$("#title_text").html("REQUESTS");
	//$("#stage").load("screens/create_researcher_request.html");
	$("#stage").load("screens/researcher_request.html");
});

Path.map("#/research-reports").to(function() {
	$("#title_text").html("REPORTS");
	$("#stage").load("screens/researcher_summary.html");
});

Path.map("#/research-templates").to(function() {
	$("#title_text").html("TEMPLATES");
	$("#stage").load("screens/researcher_templates.html");
});

Path.map("#/rest-api-explorer").to(function() {
	$("#title_text").html("REST API EXPLORER");
	$("#stage").load("screens/restapi_explorer.html");
});

Path.map("#/show-ds").to(function() {
	$("#title_text").html("DATA SETS");
	$("#stage").load("screens/researcher_create_dataset_form.html");
});

Path.map("#/compare").to(function() {
	$("#title_text").html("SUBJECT VISITS");
	$("#stage").load("screens/researcher_compare.html");
});

Path.map("#/import-ds").to(function() {
	$("#title_text").html("IMPORT DATASET");
	$("#stage").load("screens/researcher_import_dataset.html");
});

// MSKCC ROUTINGS/////////////////////////

Path.map("#/protocol-t").to(function() {
	$("#title_text").html("PROTOCOL");
	$("#stage").load("screens/Protocols-screen.html");
});
Path.map("#/protocols_table").to(function() {
	$("#title_text").html("PROTOCOL");
	$("#stage").load("screens/protocols_table.html");
});
Path.map("#/study-timepoints").to(function() {
	$("#title_text").html("STUDY TIMEPOINTS");
	$("#stage").load("screens/study_timepoints_screen.html");
});
Path.map("#/study-treatments").to(function() {
	$("#title_text").html("TREATMENTS");
	$("#stage").load("screens/study_treatments_screen.html");
});
Path.map("#/study-specimen").to(function() {
	$("#title_text").html("SPECIMEN TISSUE");
	$("#stage").load("screens/study_specimen_screen.html");
});
Path.map("#/patients_table").to(function() {
	$("#title_text").html("STUDY PATIENTS");
	$("#stage").load("screens/patients_table.html");
});
Path.map("#/timeline_evaltion").to(function() {
	$("#title_text").html("TIMELINE - EVALUATION PLAN");
	$("#stage").load("screens/timeline_evaltion.html");
});
Path.map("#/response_table").to(function() {
	$("#title_text").html("RESPONSE");
	$("#stage").load("screens/response_table.html");
});
Path.map("#/adverse-events-t").to(function() {
	$("#title_text").html("ADVERSE EVENTS");
	$("#stage").load("screens/protocols_screen_t.html");
});
Path.map("#/study-assays").to(function() {
	$("#title_text").html("ASSAYS");
	$("#stage").load("screens/study_assays.html");
});
Path.map("#/wes1").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/wes-main1-img.html");
});
Path.map("#wes2").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/wes-main2-img.html");
});
Path.map("#ngs1").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/ngs_table1.html");
});
Path.map("#/ngs2").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/ngs_table2.html");
});
Path.map("#/subject-view").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").load("screens/subject_assays.html");
});
Path.map("#/sub-wes-main1").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").load("screens/sub-wes-main1.html");
});
    
Path.map("#/sub-wes-main2").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").load("screens/sub-wes-main2.html");
});
Path.map("#/cohorting_table_1").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/cohorting_table_D.html");
});
Path.map("#/cohorting_table_2").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/cohorting_table_V.html");
});
Path.map("#/cohorting_table_3").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/cohorting_table_LG.html");
});
Path.map("#/cohorting_table_4").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/cohorting_table_NB.html");
});
Path.map("#/cohorting_dataset").to(function() {
	$("#title_text").html("STUDY VIEW");
	$("#stage").load("screens/cohorting_img.html");
});
Path.map("#/cr_subject_view").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").load("screens/subject_assays.html");
});
Path.map("#/sub-wes-table1").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").load("screens/subjectview_CR9699.html");
});
Path.map("#/sub-wes-table2").to(function() {
	$("#title_text").html("SUBJECT VIEW");
	$("#stage").load("screens/subjectview_CR9699_2.html");
});

/////////////////////////////////////

Path.root("#/login");
Path.listen();

