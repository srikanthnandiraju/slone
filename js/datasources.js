/**
 * @author swamyk
 */
var public_spreadsheet_url2 = "1v0o6Delfj2SuxicpfR-IUmeWo3Xel2PTzd8QqQx02RU", db2 = ""; 

function getUserAgent(){
	if( /iPhone|iPad|iPod|Safari/i.test(navigator.userAgent) &&  !(/Firefox|Chrome/i.test(navigator.userAgent))) {
		return true;
	}
	return false;
}

var Storage = (getUserAgent()) ? window.sessionStorage : window.localStorage;
 
(function() {
	var createdAt = sessionStorage.getItem('DS_CREATED_AT');
	var DATASOURCES = Storage.getItem('DATASOURCES');
	if (!createdAt || !DATASOURCES) {
		console.log('NO DB FOUND..CREATING FULL DS & DB');
	    initS();
		return;
	}
	createDBS();
}());

function initS() {
	Tabletop.init({
		key : public_spreadsheet_url2,
		callback : readData
	});
}

function readData(data, tabletop) {

	var cytokines = tabletop.sheets('Cytokine').all().slice(0, 2016),
	    flowcytometries = tabletop.sheets('Flow Cytometry').all().slice(0, 2016),
	    ihcs = tabletop.sheets('IHC').all().slice(0, 2016);
	
	var compareData = tabletop.sheets('Comparison').all() || []; 
	console.log('original sheet compareData length is = ' + compareData.length);
	
	Storage.setItem('DATASOURCES', JSON.stringify({
		'STUDIES' : tabletop.sheets('Studies').all(),
		'PATIENTS' : tabletop.sheets('Patients').all(),
		'SPECIMEN' : tabletop.sheets('Specimen').all(),
		'TREATMENT' : tabletop.sheets('Treatment').all(),
		'ADVERSEVENT' : tabletop.sheets('Adverse Events').all(),
		'CLINICALLABS' : tabletop.sheets('Clinical lab').all(),
		'COMPARISON' : compareData,
		'CYTOKINE' : cytokines,
		'FLOWCYTOMETRY' : flowcytometries,
		'IHC' : ihcs
	}));
	var createdAt = new Date();
	sessionStorage.setItem('DS_CREATED_AT', JSON.stringify(createdAt.getTime()));

	createDBS();
}

function createDBS() {

	var DATASOURCE = JSON.parse(Storage.getItem('DATASOURCES'));

	db2 = new loki('pici2.json');

	//db collection for STUDIES
	var DB_STUDIES = db2.addCollection('STUDIES');
	DATASOURCE.STUDIES.forEach(function(study_obj) {
		DB_STUDIES.insert(study_obj);
	});

	//db collection for PATIENTS
	var DB_PATIENTS = db2.addCollection('PATIENTS');
	DATASOURCE.PATIENTS.forEach(function(patient_obj) {
		DB_PATIENTS.insert(patient_obj);
	});

	//db collection for SPECIMEN
	var DB_SPECIMEN = db2.addCollection('SPECIMEN');
	DATASOURCE.SPECIMEN.forEach(function(specimen_obj) {
		DB_SPECIMEN.insert(specimen_obj);
	});

	//db collection for TREATMENT
	var DB_TREATMENT = db2.addCollection('TREATMENT');
	DATASOURCE.TREATMENT.forEach(function(treatment_obj) {
		DB_TREATMENT.insert(treatment_obj);
	});

	//db collection for ADVERSEVENT
	var DB_ADVERSEVENT = db2.addCollection('ADVERSEVENT');
	DATASOURCE.ADVERSEVENT.forEach(function(adverse_event_obj) {
		DB_ADVERSEVENT.insert(adverse_event_obj);
	});

	//db collection for CLINICAL LABS
	var DB_CLINICALLABS = db2.addCollection('CLINICALLABS');
	DATASOURCE.CLINICALLABS.forEach(function(Clab_obj) {
		DB_CLINICALLABS.insert(Clab_obj);
	});

	//db collection for CYTOKINE
	var DB_CYTOKINE = db2.addCollection('CYTOKINE');
	DATASOURCE.CYTOKINE.forEach(function(cytokine_obj) {
		DB_CYTOKINE.insert(cytokine_obj);
	});

	//db collection for IHC
	var DB_IHC = db2.addCollection('IHC');
	DATASOURCE.IHC.forEach(function(ihc_obj) {
		DB_IHC.insert(ihc_obj);
	});

	//db collection for FLOWCYTOMETRY
	var DB_FLOWCYTOMETRY = db2.addCollection('FLOWCYTOMETRY');
	DATASOURCE.FLOWCYTOMETRY.forEach(function(flowcytometry_obj) {
		DB_FLOWCYTOMETRY.insert(flowcytometry_obj);
	});
	
	//db collection for COMPARISON
	var DB_COMPARISON = db2.addCollection('COMPARISON');
	DATASOURCE.COMPARISON.forEach(function(compare_obj) {
		DB_COMPARISON.insert(compare_obj);
	});
}