function nctLink(url) {
		LS.set_data('NCT-URL', url);
		$('body').trigger('study-details');
}

(function($) {
		$.fn.extend({
			triggerAll : function(events, params) {
				var el = this,i,evts = events.split(' ');
				for ( i = 0; i < evts.length; i += 1) {
					el.trigger(evts[i], params);
				}
				return el;
			}
		});
}(jQuery));

function getStandardDate(date){
	if(typeof date === 'string')
			date = date.trim();
	if(!date || date === 'null')
		return '';

	var splitDate = date.split('-');
	if(!isNaN(splitDate[0]) && !isNaN(splitDate[1]) && !isNaN(splitDate[2]))
				return date;
	
	var months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
	var mm = months.indexOf(splitDate[1].toUpperCase(), 0) + 1;
	if(mm < 10)
		mm = '0' + mm;
	var yyyy = '20' + splitDate[2];
	return yyyy+'-'+mm+'-'+splitDate[0];
}

function prepareHtml(summaryTemplateId) {
	summaryTemplate = $('#' + summaryTemplateId).html();
	return function(templateData) {
		return Mustache.to_html(summaryTemplate, {
			rows : templateData
		});
	};
}

function fixScrolling() {
	var stuff = {};
	$('#center-body').on('touchstart', stuff, function(e) {
		e.data.max = this.scrollHeight - this.offsetHeight;
		e.data.y = e.originalEvent.pageY;
	}).on('touchmove', stuff, function(e) {
		var dy = e.data.y - e.originalEvent.pageY;
		// if scrolling up and at the top, or down and at the bottom
		if ((dy < 0 && this.scrollTop < 1) || (dy > 0 && this.scrollTop >= e.data.max)) {
			e.preventDefault();
		};
	});
}

$(document).ajaxStart(function() {
	NProgress.start();
});

$(document).ajaxStop(function() {
	NProgress.done();
});

function showFullScreen() {
	$('#fullscreenElement').addClass('open');
};

$('.close').on('click', function(event) {
	$('#fullscreenElement').removeClass('open');
	$('#fullscreen_stage').empty();
});

function showCustomTooltip() {
	$('#custom-tooltip').addClass('open');
};

$('.cx').on('click', function(event) {
	$('#custom-tooltip').removeClass('open');
});

function closeFullScreen() {
	$('#fullscreenElement').removeClass('open');
	$('#fullscreen_stage').empty();
}

$('#back').on('click', function(event) {
	$('#fullscreenElement').removeClass('topmargin70 open fadeInRight animated');
	$(this).removeClass('fadeInRight animated').hide();
	$('#searchitem').removeClass('fadeInRight animated').hide().val('');
	$('.search-hide').show();
});

function showSearchScreen() {
	$('#fullscreen_stage').empty();
	$('#fullscreenElement').addClass('topmargin70 open fadeInRight animated');
}

function logout() {
	LS.remove_data('EDIT_DATA');
	LS.remove_data('MINDOB');
	LS.remove_data('MAXDOB');
	LS.remove_data('MAX_DATE_RANGE');
	LS.remove_data('MIN_DATE_RANGE');
	LS.remove_data('TARGETID');
	LS.remove_data('DIRECTION');
	LS.remove_data('USER_PRIOFILE');
	$('body').trigger('login-screen');
}

$('body').bind('logged_out', function(e, data) {
	logout();
});

$('body').bind('search-by-string', function(e, data) {
	$.ajax({
		type : 'GET',
		url : base + '/search/patient/' + data,
		success : function(response) {
			if (response.ack == "success") {
				$('#fullscreenElement').removeClass('open');
				localStorage.PATIENT_DATA = JSON.stringify(response.data[0]);
				window.location.href = "#/new_patient_profile";
			} else {
				$('#error').text('no patient info found..');
			}
		},
		error : function(failure) {
			console.log(failure);
		}
	});
});

var firstWait1 = true;
var firstWait2 = true;

// Rendering Menu Items
var menuObject = {
	'bioinformatician' : [{
		menuItems : ['Work List', 'Studies', 'Research Reports', 'Data Sets', 'REST API Explorer', 'Tools', 'Archives', 'Profile', 'Logout'],
		eventItems : ['bioinformatician-home', '', 'research-report-form', '', 'rest-api-explorer', '', '', 'profile', 'logged_out'],
		iconItems : ['fa fa-list-ol', 'fa fa-book', 'fa fa-bar-chart', 'fa fa-database', 'fa fa-gear', 'fa fa-wrench', 'fa fa-archive', 'fa fa-user', 'fa fa-sign-out']
	}],
	'director' : [{
		menuItems : ['Home', 'Profile', 'Logout'],
		eventItems : ['director-home', 'profile', 'logged_out'],
		iconItems : ['fa fa-home', 'fa fa-user', 'fa fa-sign-out']
	}],
	'patient' : [{
		menuItems : ['Home', 'Schedule', 'Timeline', 'Response', 'Documents', 'Care Team', 'Profile', 'Logout'],//{index in array 6:'Treatments', event : 'patient-medication', icon : 'fa fa-file-text', }, {index in array 9:'History', event : 'patient-history', icon : 'fa fa-history',}, {index in array 6:'Images', event : 'patient-images', icon : 'fa fa-picture-o',}
		eventItems : ['patient-home', 'patient-schedule', 'patient-timeline', 'patient-response', 'patient-documents', 'patient-careteam', 'profile', 'logged_out'],
		iconItems : ['fa fa-home', 'fa fa-calendar-check-o', 'fa fa-calendar', 'fa fa-file-text-o', 'fa fa-file', 'fa fa-user-md', 'fa fa-user', 'fa fa-sign-out']
	}],
	'researcher' : [{
		menuItems : ['Compare', 'Studies', 'Research Requests', 'Research Reports', 'Import DataSet', 'Data Sets', 'REST API Explorer', 'Tools', 'Research Templates', 'Regulatory', 'Archives', 'Profile', 'Logout'],
		eventItems : ['researcher-compare', 'researcher-home', 'research-requests-screen', '', 'import-ds', 'studies-screen', 'rest-api-explorer', '', 'research-templates-screen', '', '', 'profile', 'logged_out'],
		iconItems : ['fa fa-bar-chart', 'fa fa-book', 'fa fa-search', 'fa fa-line-chart', 'fa fa-upload', 'fa fa-database', 'fa fa-gear', 'fa fa-wrench', '', 'fa fa-registered', 'fa fa-archive', 'fa fa-user', 'fa fa-sign-out']
	}],
	'clinician' : [{
		menuItems : ['Home', 'Profile', 'Logout'],
		evsentItems : ['clinician-home', 'profile', 'logged_out'],
		iconItems : ['fa fa-home', 'fa fa-user', 'fa fa-sign-out']
	}],
	'singlestudyview' : [{
		menuItems : ['Home', 'Study Home', 'Subject Home', 'Patients', 'Protocol', 'LifeStyle', 'Treatment', 'Timepoints', 'Specimen', 'Assays', 'Adverse Events', 'Response','Timeline','Cohorting', 'Profile', 'Logout'],
		eventItems : ['director-home', 'full-study-view', 'full-subject-view', 'study-patients-screen', 'protocol-t', '', 'study-treatments', 'study-timepoints', 'study-specimen', 'study-assays', 'study-adverse-screen', 'study-response-screen', 'study-timeline-screen', 'study-cohorting-screen', 'profile', 'logged_out'],
		iconItems : ['fa fa-home', 'fa fa-h-square', 'fa fa-h-square', 'fa fa-wheelchair', 'fa fa-file-powerpoint-o','', 'fa fa-file-text', 'fa fa-calendar-check-o', 'fa fa-sitemap', 'fa fa-object-group', 'fa fa-adn', 'fa fa-file-text-o', 'fa fa-calendar', 'fa fa-users' , 'fa fa-user', 'fa fa-sign-out']
	}]
};

//User Profile Details
var accountObject = {
	'bioinformatician' : [{
		role : 'Bioinformatician',
		name : 'Jake Johnson',
		title : 'Bioinformatician, Cancer Immunotherapy Treatment',
		currentProgram : 'Cancer Immunotherapy Treatment',
		profileImage : 'https://randomuser.me/api/portraits/men/24.jpg',
		username : 'bob@parkerici.org'
	}],
	'researcher' : [{
		role : 'Researcher',
		name : 'Tom Hussy',
		title : 'Researcher, Cancer Immunotherapy Treatment',
		currentProgram : 'Cancer Immunotherapy Treatment',
		profileImage : 'https://randomuser.me/api/portraits/men/27.jpg',
		username : 'rex@parkerici.org'
	}],
	'patient' : [{
		role : 'Patient',
		name : 'John Doe',
		title : 'Study: A Neoadjuvant Phase IIa Study of Ipilimumab',
		currentProgram : 'Cancer Immunotherapy Treatment',
		profileImage : 'https://randomuser.me/api/portraits/men/40.jpg',
		username : 'pat@parkerici.org'
	}],
	'director' : [{
		role : 'Director',
		name : 'John Smith',
		title : 'Program Manager, Cancer Immunotherapy Treatment',
		currentProgram : 'Cancer Immunotherapy Treatment',
		profileImage : 'https://randomuser.me/api/portraits/men/29.jpg',
		username : 'dan@parkerici.org'
	}],
	'clinician' : [{
		role : 'Clinician',
		name : 'Adam Chiko',
		title : 'Cancer Immunotherapy Treatment',
		currentProgram : 'Cancer Immunotherapy Treatment',
		profileImage : 'https://randomuser.me/api/portraits/men/31.jpg',
		username : 'cln@parkerici.org'
	}]
};

function getMenuInfo(loggedInUser) {
	if ($.inArray(loggedInUser, ['bioinformatician', 'director', 'patient', 'researcher', 'clinician']) !== -1) {
		$('#username').html(accountObject[loggedInUser][0]['username']);
		LS.set_data('USER_PRIOFILE', accountObject[loggedInUser][0]);
	}
	loggedInUser = menuObject[loggedInUser];
	
	var menu = [],currentMenu = loggedInUser[0];
	for (var i = 0; i < currentMenu.menuItems.length; i++) {
		var Obj = {};
		Obj.eventItem = currentMenu.eventItems[i];
		Obj.menuItem = currentMenu.menuItems[i];
		Obj.iconItem = currentMenu.iconItems[i];
		Obj.disableItem = (currentMenu.eventItems[i])? '' : ' disabled';
		menu.push(Obj);
	}
	return menu;
}

// SIDE NAV STUFF
// ------------------- EVENTS ON ELEMENTS -------------------------------

function showSideNav() {
	//$("#side").css("left", "0px");
	//$("#center-body").css("left", "250px");
	//$("#header").css("left", "250px");

	$("#center-body, #header").velocity({
		left : "250px",
	}, {
		duration : 100,
		easing : "easeInSine",
		sequenceQueue : false,
	});

	$("#side").velocity({
		left : "0px",
	}, {
		duration : 100,
		easing : "easeInSine"
	});

	flag = true;
}

function hideSideNav() {
	//$("#side").css("left", "-255px");
	//$("#center-body").css("left", "0px");
	//$("#header").css("left", "0px");

	$("#side").velocity({
		left : "-250px",
	}, {
		duration : 100,
		easing : "easeOutQuad"
	});

	$("#center-body, #header").velocity({
		left : "0px",
	}, {
		duration : 100,
		easing : "easeOutQuad",
		sequenceQueue : false,
	});

	flag = false;
}

function toggleNav() {
	flag = !flag;
	if (flag) {
		showSideNav();
	} else {
		hideSideNav();
	}
}

$('body').bind('hide-side-nav', function(e, data) {
	hideSideNav();
});

$('body').bind('show-side-nav', function(e, data) {
	showSideNav();
});