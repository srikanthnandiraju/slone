/**
 * @author swamyk
 */

//MSKCC related bindings
$('body').bind('protocol-t', function(e, data) {
	window.location.href = "#/protocol-t";
});
$('body').bind('study-treatments', function(e, data) {
	window.location.href = "#/study-treatments";
});
$('body').bind('timeline-t', function(e, data) {
	window.location.href = "#/sample.html";
});
$('body').bind('patients-t', function(e, data) {
	window.location.href = "#/patients-t";
});
$('body').bind('study-assays', function(e, data) {
	window.location.href = "#/study-assays";
});
$('body').bind('response-t', function(e, data) {
	window.location.href = "#/response-t";
});
$('body').bind('study-timepoints', function(e, data) {
	window.location.href = "#/study-timepoints";
});
$('body').bind('study-specimen', function(e, data) {
	window.location.href = "#/study-specimen";
});
$('body').bind('wes1', function(e, data) {
	window.location.href = "#/wes1";
});
$('body').bind('ngs2', function(e, data) {
	window.location.href = "#/ngs2";
});
$('body').bind('response_table', function(e, data) {
	window.location.href = "#/response_table";
});

$('body').bind('cohorting_table_1', function(e, data) {
	window.location.href = "#/cohorting_table_1";
});
$('body').bind('cohorting_table_2', function(e, data) {
	window.location.href = "#/cohorting_table_2";
});
$('body').bind('cohorting_table_3', function(e, data) {
	window.location.href = "#/cohorting_table_3";
});
$('body').bind('cohorting_table_4', function(e, data) {
	window.location.href = "#/cohorting_table_4";
});
$('body').bind('create-dataset-screen', function(e, data) {
	window.location.href = "#/cohorting_dataset";
});

$('body').bind('CR9999', function(e, data) {
	window.location.href = "#/cr_subject_view";
});

$('body').bind('sub-wes-main1', function(e, data) {
	window.location.href = "#/sub-wes-main1";
});
$('body').bind('sub-wes-main2', function(e, data) {
	window.location.href = "#/sub-wes-main2";
});
$('body').bind('sub-wes-table1', function(e, data) {
	window.location.href = "#/sub-wes-table1";
});
$('body').bind('sub-wes-table2', function(e, data) {
	window.location.href = "#/sub-wes-table2";
});


$('body').bind('show-event-details', function(e, data) {
	showCustomTooltip();
});

//login related bindings

$('body').bind('login-screen', function(e, data) {
	window.location.href = "#/login";
});

$('body').bind('profile', function(e, data) {
	window.location.href = "#/profile";
});

$('body').bind('register-screen', function(e, data) {
	window.location.href = "#/signup";
});

//Clinician Related Bindings
$('body').bind('clinician-home', function(e, data) {
	window.location.href = "#/clinician-home";
});

// Director Related Bindings

$('body').bind('director-home', function(e, data) {
	window.location.href = "#/director-home";
});

$('body').bind('full-study-view', function(e, data) {
	//console.log('study id is == ' + studyId);
	if(data) LS.set_data('STUDYID', data);
	window.location.href = "#/study-view";
});

$('body').bind('full-subject-view', function(e, data) {
	//console.log('study id is == ' + studyId);
	if(data) LS.set_data('STUDYID', data);
	window.location.href = "#/single-subject-view";
});
$('body').bind('single-patient-view', function(e, data) {
	window.location.href = "#/single-patient-view";
});

$('body').bind('compare-screen', function(){
	$("#fullscreen_stage").load("screens/patient_visit_compare.html");
	showFullScreen();
});

$('body').bind('study-patients-screen', function(){
	window.location.href = "#/study-patients";
});

$('body').bind('study-protocol-screen', function(){
	window.location.href = "#/study-protocol";
});

$('body').bind('study-adverse-screen', function(){
	window.location.href = "#/study-adverse";
});

$('body').bind('study-response-screen', function(){
	window.location.href = "#/study-response";
});

$('body').bind('study-timeline-screen', function(){
	window.location.href = "#/study-timeline";
});

$('body').bind('study-cohorting-screen', function(){
	window.location.href = "#/study-cohort";
});

//Patient Menu Items related bindings

$('body').bind('patient-home', function(e, data) {
	window.location.href = "#/patient-home";
});

$('body').bind('patient-history', function(e, data) {
	window.location.href = "#/patient-history";
});

$('body').bind('patient-careteam', function(e, data) {
	window.location.href = "#/patient-careteam";
});

$('body').bind('patient-images', function(e, data) {
	window.location.href = "#/patient-images";
});

$('body').bind('patient-medication', function(e, data) {
	window.location.href = "#/patient-medication";
});

$('body').bind('patient-documents', function(e, data) {
	window.location.href = "#/patient-documents";
});

$('body').bind('patient-response', function(e, data) {
	window.location.href = "#/patient-response";
});

$('body').bind('patient-timeline', function(e, data) {
	window.location.href = "#/patient-timeline";
});

$('body').bind('patient-schedule', function(e, data) {
	window.location.href = "#/patient-schedule";
});

$('body').bind('patient-details1', function(e, data) {
	$("#fullscreen_stage").load("screens/patient_details.html");
	showFullScreen();
});

$('body').bind('study-details', function(e, data) {
	$("#fullscreen_stage").load("screens/study_details.html");
	showFullScreen();
});

//Bio-Informatician Related bindings


$('body').bind('bioinformatician-home', function(e, data) {
	window.location.href = "#/bioinformatician-home";
});

$('body').bind('bio-work-list', function(e, data) {
	window.location.href = "#/bio-work-list";
});

$('body').bind('research-report-form', function(e, data) {
	window.location.href = "#/research-report-form";
});

$('body').bind('research-reports', function(e, data) {
	window.location.href = "#/research-reports";
});

$('body').bind('rest-api-explorer', function(e, data) {
	$("#restapi-explorer-id").addClass('disabled');
	$("#bio-research-req-id").removeClass('disabled');
	window.location.href = "#/rest-api-explorer";
});

// Researcher screens

$('body').bind('researcher-home', function(e, data) {
	window.location.href = "#/researcher-home";
});

$('body').bind('studies-screen', function(e, data) {
	window.location.href = "#/research-datasets";
});

$('body').bind('research-requests-screen', function(e, data) {
	window.location.href = "#/research-requests";
});

$('body').bind('research-templates-screen', function(e, data) {
	window.location.href = "#/research-templates";
});

$('body').bind('create-ds', function(e, data) {
	window.location.href = "#/show-ds";
});

$('body').bind('import-ds', function(e, data) {
	window.location.href = "#/import-ds";
});

$('body').bind('researcher-compare', function(e, data) {
	window.location.href = "#/compare";
});

$('body').bind('show-template', function(e, data){
	$("#fullscreen_stage").load("screens/researcher_template_view.html");
	showFullScreen();
});

$('body').bind('create-research-request-screen', function(e, data){
	$("#fullscreen_stage").load("screens/create_researcher_request.html");
	showFullScreen();
});
$('body').bind('create-template-screen', function(e, data){
	$("#fullscreen_stage").load("screens/create_researcher_template.html");
	showFullScreen();
});

//In Single Patient View, more link related bindings

$('body').bind('cytokine-screen', function(e, data) {
	$("#title_text").html("CYTOKINE");
	window.location.href = "#/cytokine-details";
});

$('body').bind('ihc-screen', function(e, data) {
	$("#title_text").html("IHC");
	window.location.href = "#/ihc-details";
});

$('body').bind('flowcytometry-screen', function(e, data) {
	$("#title_text").html("FLOW CYTOMETRY");
	window.location.href = "#/flow-cytometry-details";
});

$('body').bind('adverse-screen', function(e, data) {
	$("#title_text").html("ADVERSE EVENTS");
	window.location.href = "#/adverse-events-details";
});

$('body').bind('treatments-screen', function(e, data) {
	$("#title_text").html("TREATMENTS");
	window.location.href = "#/treatments-details";
});

$('body').bind('collection-screen', function(e, data) {
	$("#title_text").html("COLLECTIONS");
	window.location.href = "#/collection-details";
});
$('body').bind('clinical-labs-screen', function(e, data) {
	$("#title_text").html("CLINICAL LABS");
	window.location.href = "#/clinical-labs-details";
});

$('body').bind('clinical-history-screen', function(e, data) {
	$("#title_text").html("CLINICAL HISTORY");
	$("#stage").load("screens/clinical_history_screen.html");
});

$('body').bind('spv-tl', function(e, data) {
	$("#fullscreen_stage").load("screens/patient_timeline.html");
	showFullScreen();
});

$('body').bind('send-mail', function(e, data) {
	$("#fullscreen_stage").load("screens/patient_schedule_events_mailer.html");
	showFullScreen();
});

/*
$('body').bind('collection-screen', function(e, data) {
	$("#title_text").html("COLLECTIONS");
	$("#stage").load("screens/collection_screen.html");
});

$('body').bind('clinical-labs-screen', function(e, data) {
	$("#title_text").html("CLINICAL LABS");
	$("#stage").load("screens/clinical_labs_screen.html");
});

$('body').bind('treatments-screen', function(e, data) {
	$("#title_text").html("TREATMENTS");
	$("#stage").load("screens/treatment_screen.html");
});

$('body').bind('adverse-screen', function(e, data) {
	$("#title_text").html("ADVERSE EVENTS");
	$("#stage").load("screens/adverse_screen.html");
});

*/

$('body').bind('microrna-screen', function(e, data) {
	$("#title_text").html("MICRO RNA");
	$("#stage").load("screens/microrna_screen.html");
});

$('body').bind('search-screen', function(e, data) {
	$("#fullscreen_stage").load("screens/search_form.html");
	showFullScreen();
});



$('body').bind('pathology-view', function(e, data) {
	$("#fullscreen_stage").load("screens/pathology_view.html");
	showFullScreen();
});

//Flip screens
$('body').bind('flow-flip', function(e, data) {
	$("#fullscreen_stage").load("screens/flow_flip.html");
	showFullScreen();
});

$('body').bind('ihc-flip', function(e, data) {
	$("#fullscreen_stage").load("screens/ihc_flip.html");
	showFullScreen();
});

$('body').bind('cyto-flip', function(e, data) {
	$("#fullscreen_stage").load("screens/cyto_flip.html");
	showFullScreen();
});

// Load ihc,cytokine and flow-cytometry popup data source 
$('body').bind('cytokine-details', function(e, data) {
	$("#fullscreen_stage").load("screens/cytokine_screen.html");
	showFullScreen();
});
$('body').bind('ihc-details', function(e, data) {
	$("#fullscreen_stage").load("screens/ihc_screen.html");
	showFullScreen();
});
$('body').bind('flow-cytometry-details', function(e, data) {
	$("#fullscreen_stage").load("screens/flow_cytometry_screen.html");
	showFullScreen();
});

// $('body').bind('show-menu', function(e, data) {
	// $("#fullscreen_stage").load("screens/" + LS.get_data('LOGGEDINUSER') + "_menu.html");
	// showFullScreen();
// });
// $('body').bind('close-screen', function(e, data) {
	// closeFullScreen();
// });

//-----------------------------------------------------------

// $('body').bind('bio-research-req-view', function(e, data) {
	// $("#restapi-explorer-id").removeClass('disabled');
	// $("#bio-research-req-id").addClass('disabled');
	// $("#bio-research-request").load("screens/bio_view_research_requests.html");
// });

// $('body').bind('view-research-requests', function(e, data) {
	// $("#create-request-id").removeClass('disabled');
	// $("#view-request-id").addClass('disabled');
	// $("#research-request").load("screens/view_research_requests.html");
// });

// $('body').bind('create-new-request', function(e, data) {
	// $("#create-request-id").addClass('disabled');
	// $("#view-request-id").removeClass('disabled');
	// $("#research-request").load("screens/create_new_request_form.html");
// });

/*
 $('body').bind('home-screen', function(e, data) {
 localStorage.setItem('activePage','home-screen');
 $("#title_text").html("CLINICAL TRIAL DASHBOARD");
 $("#stage").load("screens/home.html");
 });*/

// $('body').bind('add-patient-to-study', function(e, data) {
	// $("#fullscreen_stage").load("screens/create_patient_form.html");
	// showFullScreen();
// });

// $('body').bind('cytokine-trend-screen', function(e, data) {
	// $("#fullscreen_stage").load("screens/cytokine_trend_screen.html");
	// showFullScreen();
// });

